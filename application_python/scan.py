import scapy.all as scapy
import socket
import requests

# Liste globale pour stocker les résultats
all_scan_results = []

def get_local_ip():
    try:
        # Créer un socket et obtenir l'adresse IP locale
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        local_ip = s.getsockname()[0]
        s.close()
        return local_ip
    except Exception as e:
        print(f"Error getting local IP address: {e}")
        return None

def scan(ip):
    arp_request = scapy.ARP(pdst=ip)
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast / arp_request

    answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]
    clients_list = []

    for element in answered_list:
        client_dict = {"ip": element[1].psrc, "mac": element[1].hwsrc}
        clients_list.append(client_dict)

    return clients_list

def display_result(results):
    scan_results = []

    # Collecter les résultats dans une liste
    for client in results:
        open_ports = find_open_ports(client['ip'])
        hostname = found_hostname(client['ip'])
        scan_results.append((client['ip'], client['mac'], hostname, open_ports))

    # Afficher les résultats avec une mise en forme correcte
    print("IP Address\t\tMAC Address\t\tHostname\t\tOpen Ports")
    print("-----------------------------------------------------------")
    for result in scan_results:
        ip, mac, hostname, open_ports = result
        print(f"{ip.ljust(15)}{mac.ljust(20)}{hostname.ljust(20)}{open_ports}")

def scan_and_display(ip):
    scan_result = scan(ip)
    indexed_scan_result = [{'index': index, 'ip': client['ip'], 'mac': client['mac'], 'hostname': found_hostname(client['ip']), 'open_ports': []} for index, client in enumerate(scan_result, start=1)]
    all_scan_results.extend(indexed_scan_result)  # Ajouter les résultats actuels à la liste globale
    display_result(scan_result)

    # Envoie les résultats de numérisation au serveur
    send_results_to_server(indexed_scan_result)
    return scan_result

def send_results_to_server(results):
    url = 'http://localhost:5001/scan'
    response = requests.post(url, json=results)
    if response.status_code == 200:
        print('Scan results sent successfully.')
    else:
        print('Failed to send scan results to server.')

def found_hostname(ip_address):
    try:
        result = socket.gethostbyaddr(ip_address)
        return result[0]
    except socket.herror as e:
        if e.errno == 11004:
            return "Unknown"
        else:
            return f"Error: {e}"

def find_open_ports(ip_address, ports=(80, 443)):
    open_ports = []
    for port in ports:
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.settimeout(1)
                print(f"Scanning port {port} on {ip_address}")
                result = s.connect_ex((ip_address, port))
                if result == 0:
                    print(f"Port {port} is open on {ip_address}")
                    open_ports.append(str(port))
                else:
                    print(f"Port {port} is closed on {ip_address}")
        except Exception as e:
            print(f"Error scanning port {port}: {e}")
    return ', '.join(open_ports)

def clear_results():
    print("\nResults cleared.\n")

def main():
    while True:
        print("\nScanning local network in progress...\n")
        local_ip = get_local_ip()
        if local_ip:
            ip_range = local_ip.rsplit('.', 1)[0] + ".1/24"  # Crée une plage d'adresses IP locales à scanner
            scan_and_display(ip_range)
            # À ce stade, all_scan_results contiendra tous les résultats du scan
        else:
            print("Failed to get local IP address. Exiting...")

        repeat = input("Voulez-vous répéter le scan ? (oui/non): ").lower()
        if repeat != 'oui':
            break

if __name__ == "__main__":
    main()
