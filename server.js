const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();
const port = 5001;
let scanResults = [];

// Middleware to parse JSON requests
app.use(bodyParser.json());

// Serve static files from the 'public' directory
app.use(express.static(path.join(__dirname, 'public')));

// Route to serve the scan results page
app.get('/scan', (req, res) => {
  // Generate HTML markup for displaying the scan results
  let html = `
  <html>
  <head>
    <title>Scan Results</title>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <div class="navbar">
      <a href="#home">Home</a>
      <a href="#scan">Scan Results</a>
      <a href="#contact">Contact</a>
    </div>
    <h1>Tableau de bord Client 1 - Adresse IP Locale : scan_result</h1>
    <div class="scan-time">Last Scan Time: 03/07/2024 08:58:33</div>
    <div class="button-container">
      <button class="button" onclick="refreshScan()">Refresh</button>
      <button class="button" onclick="exportScan()">Export</button>
    </div>
    <table>
      <thead>
        <tr>
          <th>Adresse IP</th>
          <th>Nom d'hôte</th>
          <th>Hostname_Type</th>
          <th>Protocole</th>
          <th>Port</th>
          <th>Service</th>
          <th>Statut</th>
          <th>Open Ports</th>
        </tr>
      </thead>
      <tbody>
  `;

  if (scanResults.length === 0) {
    html += '<tr><td colspan="8">Failed to load data: Failed to fetch</td></tr>';
  } else {
    // Loop through scan results and create rows in the table
    scanResults.forEach(result => {
      html += `<tr>
        <td>${result.ip || ''}</td>
        <td>${result.hostname || ''}</td>
        <td>${result.hostname_type || ''}</td>
        <td>${result.protocol || ''}</td>
        <td>${result.port || ''}</td>
        <td>${result.service || ''}</td>
        <td>${result.status || ''}</td>
        <td>${result.open_ports ? result.open_ports.join(', ') : ''}</td>
      </tr>`;
    });
  }

  html += `
      </tbody>
    </table>
    <div class="footer">
      <p>&copy; 2024 Your Company. All rights reserved.</p>
    </div>
    <script>
      function refreshScan() {
        // Code to refresh the scan results
        location.reload();
      }
      
      function exportScan() {
        // Code to export the scan results
        const dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(${JSON.stringify(scanResults)}, null, 2));
        const downloadAnchorNode = document.createElement('a');
        downloadAnchorNode.setAttribute("href", dataStr);
        downloadAnchorNode.setAttribute("download", "scan_results.json");
        document.body.appendChild(downloadAnchorNode);
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
      }
    </script>
  </body>
  </html>`;

  // Send HTML response
  res.send(html);
});

// Route to receive scan results
app.post('/scan', (req, res) => {
  // Get the scan results from the request body
  const newScanResults = req.body;
  // Add the new results to the existing list
  scanResults.push(...newScanResults);
  // Display the scan results
  res.send(`<pre>${JSON.stringify(scanResults, null, 2)}</pre>`);
});

// Start the server
app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
