const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 4000;
let scanResults = [];

// Middleware pour parser les requêtes JSON
app.use(bodyParser.json());

// Route to serve the scan results page
app.get('/scan', (req, res) => {
  // Generate HTML markup for displaying the scan results
  let html = `
  <html>
  <head>
    <title>Scan Results</title>
    <link rel="stylesheet" type="text/css" href="public/style.css">
  </head>
  <body>
    <h1>Tableau de bord Client 1 - Adresse IP Locale : scan_result</h1>
    <table>
      <thead>
        <tr>
          <th>Adresse IP</th>
          <th>Nom d'hôte</th>
          <th>Hostname_Type</th>
          <th>Protocole</th>
          <th>Port</th>
          <th>Service</th>
          <th>Statut</th>
        </tr>
      </thead>
      <tbody>
  `;

  if (scanResults.length === 0) {
    html += '<tr><td colspan="7">Failed to load data: Failed to fetch</td></tr>';
  } else {
    // Loop through scan results and create rows in the table
    scanResults.forEach(result => {
      html += `<tr>
        <td>${result.ip}</td>
        <td>${result.hostname}</td>
        <td>${result.hostname_type}</td>
        <td>${result.protocol}</td>
        <td>${result.port}</td>
        <td>${result.service}</td>
        <td>${result.status}</td>
      </tr>`;
    });
  }

  html += `
      </tbody>
    </table>
  </body>
  </html>`;

  // Send HTML response
  res.send(html);
});

// Route pour recevoir les résultats de numérisation
app.post('/scan', (req, res) => {
  // Récupérer les résultats de numérisation depuis le corps de la requête
  const newScanResults = req.body;
  // Ajouter les nouveaux résultats à la liste existante
  scanResults.push(...newScanResults);
  // Afficher les résultats de numérisation
  res.send(`<pre>${JSON.stringify(scanResults, null, 2)}</pre>`);
});
app.use(express.static('public')); // Serve static files from 'public' directory

// Démarrer le serveur
app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
