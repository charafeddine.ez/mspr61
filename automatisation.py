import subprocess
import time
import webbrowser
import requests



# Lancer le serveur server.js
print("Lancement du serveur server.js...")
server_process = subprocess.Popen(["node", "server.js"])

# Attendre quelques secondes pour que le serveur démarre complètement
time.sleep(2)

# Exécuter le scan
print("Lancement du scan...")
scan_process = subprocess.Popen(["python", "application_python/scan.py"])

# Attendre la fin du scan
scan_process.wait()


